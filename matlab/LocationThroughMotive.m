%this code plots the path of the rigid body with the help on motive.
close all
clear all
clc
i=1;
tic
while(toc<inf)
MotiveSub=rossubscriber('/Robot_1/pose');
MotiveMsg=rosmessage(MotiveSub);
MotiveMsg=receive(MotiveSub);
MotivePose(i,:)=[MotiveMsg.Pose.Position.X, MotiveMsg.Pose.Position.Y, MotiveMsg.Pose.Position.Z];
xlabel('x [m]');
ylabel('y[m]');
zlabel('z[m]');
plot3(MotivePose(:,1),MotivePose(:,2),MotivePose(:,3), 'b')
hold on
i=i+1;
end
