/* ---------------------------------------------------------------------------
 *
 * Autonomous Navigation and Perception Lab (ANPL),
 * Technion, Israel Institute of Technology,
 * Faculty of Aerospace Engineering,
 * Haifa, Israel, 32000
 * All Rights Reserved
 * See LICENSE for the license information
 *
 * -------------------------------------------------------------------------- */

/**
 * @file: pose_tracker_node.cpp
 * @brief:
 * @author:
 *
 */

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <vector>
#include "std_msgs/String.h"
#include <sstream>
#include <math.h>
#include <iostream>
#include <fstream>

std::vector<double> mocap_pose;
std::vector<double> robot_pose;
double Time_Stamp;

void PoseCallback_Robot1(const nav_msgs::Odometry::ConstPtr& Position)
{
    ROS_INFO("The robot is located at : [%f,%f,%f]", Position->pose.pose.position.x,Position->pose.pose.position.y,Position->pose.pose.position.z);
    mocap_pose.push_front(Position->pose.pose.position.z);
    mocap_pose.push_front(Position->pose.pose.position.y);
    mocap_pose.push_front(Position->pose.pose.position.x);
}

void PoseCallback_RobotA(const nav_msgs::Odometry::ConstPtr& pose)
{
    ROS_INFO("The robot is located at : [%f,%f,%f]", pose->pose.pose.position.x,pose->pose.pose.position.y,pose->pose.pose.position.z);
    ROS_INFO("im here");
    robot_pose.push_front(pose->pose.pose.position.z);
    robot_pose.push_front(pose->pose.pose.position.y);
    robot_pose.push_front(pose->pose.pose.position.x);
    Time_Stamp=pose->pose.header.time;
}
void Store_Error_In_File()
{
    ofstream pose_file;
    pose_file.open("Position_Error_Data");
    pose_file << "Error in position is x,y,z:"<<Position_Error[1] <<Position_Error[2]<<Position_Error[3];
    pose_file.close();

    ofstream time;
    time.open("Time_Data");
    time << "The Time stamps are \n"<<Time_Stamp;
    time.close();
}

int main(int argc, char** argv)
{
    std::vector<double> Position_Error;
    std::vector<double> EstimatedPosition;
    ros::init(argc, argv, "n");
    ros::NodeHandle n;
    while(ros::ok)
    {
        ros::spinOnce();
        ros::Subscriber sub_Robot_A = n.subscribe("/Robot_A/rosaria/pose", 1, PoseCallback_RobotA);
        ros::Subscriber sub_Robot_1 = n.subscribe("/Robot_1/pose", 1, PoseCallback_Robot1);
        Position_Error[1] = robot_pose[1] - mocap_pose[1];
        Position_Error[2] = robot_pose[2] - mocap_pose[2];
        Position_Error[3] = robot_pose[3] - mocap_pose[3];
    }
    return 0;

}
